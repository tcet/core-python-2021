'''
#General syntax of exception handling
try:
    block of code to try
except exception1:
    message to be shown if exception1 occurs
except exception2:
    message to be shown if exception2 occurs   
else:
    message to show if exception does not occurs
'''

num1 = int(input("enter the 1st number: "))
num2 = int(input("enter the 2nd number: "))

try:
    res = num1/num2
except ZeroDivisionError:
    print("can not divide by zero")
else:
    print(res)

try:
    print(x)
except: # handles any error that occurs while excuteing code of try block
    print("An exception occured")

    
