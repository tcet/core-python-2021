num = int(input("Enter number to print table: "));

for i in range(1, 11):
    if i==3 or i==5:
        #break
        continue
    print(f"{num} * {i} = {num * i}");
