from tkinter import *
from tkinter import messagebox

"""
To install module
sudo apt-get install python3-tk
for windows
python -m pip install python3-tk
pip install tk
"""

master = Tk()

#Label to display labels
Label(master, text='Number 1').grid(row=0);
Label(master, text='Number 2').grid(row=1);

#Entry to display textboxs
e1 = Entry(master);
e2 = Entry(master);

#to place textboxes in grid
e1.grid(row=0, column=1)
e2.grid(row=1, column=1)

#callback function getAddition to invode when user click Add button
def getAddition():
    x = int(e1.get());
    y = int(e2.get());
    messagebox.showinfo("Addition", f"Addition of {x} and {y} is {x + y}")

#Button to handle event
B = Button(master, text = "Add", command = getAddition)
B.grid(row = 2, column=1);

master.mainloop()

