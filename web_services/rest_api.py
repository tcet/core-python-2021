from flask import Flask, request, jsonify, make_response
from flask_restful import Resource, Api
from flask_cors import CORS
from functools import wraps
import pymysql

app = Flask(__name__)
api = Api(app)

#define customre decorator @authorize
def authorize(f):
	@wraps(f)
	def auth(*args, **kwargs):
		err_msg = "Authorization is required"
		
		if(request.authorization == None):
			return make_response('Not Authorized', 403, {'WWW-Authenticate' : err_msg})
			
		u_name = request.authorization.username
		pwd = request.authorization.password
		
		if (u_name == 'admin' and pwd == 'admin@123'):
			print('Correct username and password. you are authorized to use api')
			return f(*args, **kwargs) 
		
		return make_response('Not Authorized', 403, {'WWW-Authenticate' : err_msg})
		
	return auth

class MyApi(Resource):
	method_decorators = [authorize]
	
	def __init__(self):
		print("Constructor called...")
    
	
	def get(self):
		# To connect MySQL database
		conn = pymysql.connect(host='localhost', user='root', password="", db='company')
		#conn = pymysql.connect(host='localhost', user='root', password = "", db='company')		
		
		cur = conn.cursor()
		cur.execute("select * from employee")
		output = cur.fetchall()

		#print(type(output)); #this will print tuple	

		#for rec in output:
			#print(rec);
			
		# To close the connection
		conn.close()
				
		return jsonify(output);
        
	def post(self):
		data = request.get_json();
		name = data['name']
		phn = data['phone']
		adrs = data['address']
		
		# To connect MySQL database
		conn = pymysql.connect(host='localhost', user='root', password="", db='company')
		
		cur = conn.cursor()
		#sql = "INSERT INTO `employee` (`name`, `phone`, `address`, `added_date`, `updated_date`) VALUES ('priya', '87347', 'mumbai', NOW(), NOW())"
		sql = f"INSERT INTO `employee` (`name`, `phone`, `address`, `added_date`, `updated_date`) VALUES ('{name}', {phn}, '{adrs}', NOW(), NOW())"
        
		print(sql)
		cur.execute(sql)
		conn.commit()
		
		conn.close()
		return data
		
	def put(self):
		data = request.get_json();
		id = data['id']
		salary = data['salary']
		role = data['role']
		
		# To connect MySQL database
		conn = pymysql.connect(host='localhost', user='root', password="", db='company')
		
		cur = conn.cursor()
		
		sql = f"UPDATE `employee` SET `salary` = {salary}, `role` = '{role}' WHERE `employee`.`id` = {id}"
		print(sql)
		
		cur.execute(sql)
		conn.commit()
		
		conn.close()
		return data
    
	def delete(self):
		id = int(request.args.get('id'));
		
		# To connect MySQL database
		conn = pymysql.connect(host='localhost', user='root', password="", db='company')
		
		cur = conn.cursor()
		
		sql = f"DELETE FROM `employee` WHERE `employee`.`id` = {id}"
		print(sql)
		
		cur.execute(sql)
		conn.commit()
		
		conn.close()
		return f"record for id {id} is deleted"
    
    
api.add_resource(MyApi, '/myapiurl')

if __name__ == "__main__":
    app.run(debug=True)