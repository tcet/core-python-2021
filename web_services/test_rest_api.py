#from filename import app
from rest_api import app
import unittest
import base64

class RestAPITest(unittest.TestCase):
    
	def test_status(self):
		tester = app.test_client(self)
		#response = tester.get('/myapiurl')
		creds = base64.b64encode(b"admin:admin@123").decode("utf-8")
		response = tester.get('/myapiurl', headers={"Authorization": f"Basic {creds}"})
		
		self.assertEqual(response.status_code, 200)
		
	def test_content_type(self):
		tester = app.test_client(self)
		#response = tester.get('/myapiurl')
		creds = base64.b64encode(b"admin:admin@123").decode("utf-8")
		response = tester.get('/myapiurl', headers={"Authorization": f"Basic {creds}"})
		
		self.assertEqual(response.content_type, "application/json")
		
	def test_content(self):
		tester = app.test_client(self)
		#response = tester.get('/myapiurl')        
		creds = base64.b64encode(b"admin:admin@123").decode("utf-8")
		response = tester.get('/myapiurl', headers={"Authorization": f"Basic {creds}"})
			
		self.assertTrue(b'Gupta' in response.data)
   
if __name__ == "__main__":
	unittest.main()