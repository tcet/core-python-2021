from flask import Flask, jsonify, request, render_template

app = Flask(__name__)

@app.route('/')
def index():
	#return "Hello World!"
	data = {'company_name': "TCET"}
	#return data;
	return render_template('hello_world.html', data=data)

@app.route('/sqr', methods=['GET'])	
def getSqr():
	num1 = int(request.args.get('num'));
	return f"square of {num1} is {num1 * num1}"
	
@app.route('/add', methods=['GET'])
def add():
    num1 = int(request.args.get('num1'));
    num2 = int(request.args.get('num2'));
    
    return f"{num1} + {num2} = {num1 + num2}";
	
@app.route('/sub', methods=['POST'])
def sub():
    num1 = int(request.form.get('num1'));
    num2 = int(request.form.get('num2'));
    
    return f"{num1} - {num2} = {num1 - num2}";

@app.route('/cube', methods=['POST'])
def cube():
    num1 = int(request.form.get('num1'));
    
    return f"cube of {num1} is {num1 * num1 * num1}";

#read raw json	
@app.route('/mul', methods=['POST'])
def mul():
	raw_json = request.get_json();
	num1 = int(raw_json['num1']);
	num2 = int(raw_json['num2']);
    
	return f"{num1} * {num2} = {num1 * num2}";
	#print(raw_json)
	#return raw_json;

	
if __name__ == "__main__":
    app.run(debug=True);