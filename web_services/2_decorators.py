from functools import wraps

def myDecorator(f):
	@wraps(f)
	def msg(*args, **kwargs):
		print("Hello")
		print("from custom decorator")
		return f(*args, **kwargs) #add(3,6), student()
	return msg	
	
#msg()

@myDecorator
def add(x,y):
	#msg()
	return x+y
	
print(add(3,6))

@myDecorator
def sqr(x):
	#msg()
	return x*x
	
print(sqr(6))

@myDecorator
def student(name="priya", city="nagpur"):
	print(name)
	print(city)
	
student()
	
