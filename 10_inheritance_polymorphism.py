class RBI:
    def __init__(self,bal): 
        self.balance = bal
        print(f"opening Account with balance:{self.balance}")

    def showBalance(self):
        print(f"Available Balance:{self.balance}")

    def deposit(self,amt):
        self.balance = self.balance + amt
        print(f"deposit amount:{amt}")

    def withdraw(self,amt):
        self.balance = self.balance - amt
        print(f"withdraw amount:{amt}")

class HDFC(RBI):
    def withdraw(self,amt):
        self.balance = self.balance - amt - 20
        print(f"withdraw amount:{amt}")
        print("20 rs deducted as transaction fee")
        
    def applyCC(self):
        print("Applied for credit card")
    
   
robj = RBI(5000)
robj.showBalance()
robj.deposit(1000)
robj.showBalance()
robj.withdraw(200)
robj.showBalance()

hobj = HDFC(1000)
hobj.deposit(500)
hobj.showBalance()
hobj.withdraw(100)
hobj.showBalance()
hobj.applyCC()


