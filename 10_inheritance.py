class RBI:

    def __init__(self, bal):
        self.balance = bal;
        print(f"Opening Account with balance: {self.balance}");

    def showBalance(self):
        print(f"Available Balance: {self.balance}");

    def deposit(self, amt):
        self.balance = self.balance + amt;
        print(f"Diposit Amount: {amt}");

    def withdraw(self, amt):
        self.balance = self.balance - amt;
        print(f"Withdraw Amount: {amt}");

class HDFC(RBI):
    pass

hobj = HDFC(1000);
hobj.showBalance();

hobj.deposit(500);
hobj.showBalance();

hobj.withdraw(300);
hobj.showBalance();
