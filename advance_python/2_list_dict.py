# list of dict, list of list, list of tuple
import pprint 
# cmd to install pprint module: pip install pprint

pp = pprint.PrettyPrinter(indent=4)

'''
cities = ("hyderabad", "mumbai", "aurangabad")
print("============cities touple==============")
print(cities)
#itegrating on list using for loop
for city in cities:
	print(city)

student = {"name" : "samay", "city" : "mumbai",	"age" : 18}
#print(student["name"])

#list of dict
#students = [{dict1}, {dict2}, {dict3}]
students = [
		{"name":"prashant", "city":"pune", "age":19},
		{"name":"chaitali", "city":"mumbai", "age":18},
		{"name":"samay", "city":"nashik", "age":20},
		
]

# add a record to list of dict
#new_rec = {"name":"priyanka", "city":"nagpur", "age":20}
#students.append(new_rec)

#print(type(students[0]))
print("---" * 20)
pp.pprint(students)
print()
#print(students)
print("---" * 20)

print()
print(students[0]["name"])
print(students[1]["city"])
print()


for record in students:
	#print("------printing each record in list of dict------")
	#print(record)
	print("------printing only name of each record in list of dict------")
	print(record["name"])

print()
#list of list
#list_of_list = [[list1], [list2], [list3]]
food = [
	['brinjal', 'potato', 'beetroot'], 
	['apple', 'grapes', 'pipenapple'], 
	['milk', 'butter', 'curd']
]
print()
pp.pprint(food)
print()
#print(food[0])
for list in food:
	print(list[2])
'''

students = [
		{
			"name" : {
						"f_name" : "samay", 
						"l_name" : "kasliwal"
					}, 
			"city" : "pune", 
			"age" : 19
		},
		{
			"name" : {
						"f_name" : "prashant", 
						"l_name" : "gupta"
					}, 
			"city" : "mumbai", 
			"age" : 18
		},
		{
			"name" : {
						"f_name" : "chaitali", 
						"l_name" : "mishra"
					}, 
			"city" : "nashik", 
			"age" : 20
		},		
]
#rec = {"name": {"f_name":"samay", "l_name":"kasliwal"}}
pp.pprint(students)

print("---" * 20)

for student in students:
	frist_name = student['name']['f_name']
	last_name = student['name']['l_name']
	
	print(f"student name: {frist_name} {last_name}")
	#print(f"{student['name']['f_name']} {student['name']['l_name']}")
	
