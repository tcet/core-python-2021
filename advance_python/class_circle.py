class Circle:
    def __init__(self, r):
        self.radius = r;

    def area(self):
        print(f"Area of circle: {3.14 * self.radius * self.radius}");

    def circumference(self):
        print(f"Circumference of circle: {round(2 * 3.14 * self.radius, 2)}");
'''
cobj = Circle(7);
cobj.area();
cobj.circumference();
'''
class Wish:
	def __init__(self, g, n):
		self.greet = g;
		self.name = n;
		
	def printWish(self):
		print(f"Hi {self.name},  {self.greet}")
'''		
wobj = Wish("happy diwali", "samay")
wobj.printWish()
'''