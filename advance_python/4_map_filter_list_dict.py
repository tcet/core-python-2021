students = [
		{
			"name" : {
						"f_name" : "samay", 
						"l_name" : "kasliwal"
					}, 
			"city" : "pune", 
			"age" : 19
		},
		{
			"name" : {
						"f_name" : "prashant", 
						"l_name" : "gupta"
					}, 
			"city" : "mumbai", 
			"age" : 18
		},
		{
			"name" : {
						"f_name" : "chaitali", 
						"l_name" : "mishra"
					}, 
			"city" : "nashik", 
			"age" : 20
		},	
		{
			"name" : {
						"f_name" : "tom", 
						"l_name" : "richard"
					}, 
			"city" : "mumbai", 
			"age" : 21
		},
]

def getName(record):
	first_name = record['name']['f_name']
	last_name = record['name']['l_name']
	name = first_name + " " +last_name
	return name
	#return record['name']

std_names = map(getName, students)
print(list(std_names))

def checkCity(record):
	return record['city'] == "mumbai"

mumbai_students = filter(checkCity, students)
print(list(mumbai_students))