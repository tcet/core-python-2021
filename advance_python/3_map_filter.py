def getSquare(x):
	return x*x	
#print(getSquare(2))

nums = [1, 2, 3, 4, 5, 6]
print(nums)

print("-----New list using map()------")
# syntax map(function_name, list_name)
sqr_numbers = map(getSquare, nums)
print(list(sqr_numbers))

'''
#make new list using for loop
sqr_nums = []
for x in nums:
	sqr = x*x
	sqr_nums.append(sqr)
	#print(sqr)
print(sqr_nums)
'''

#filter () -> creates new list based on condition
def getEven(x):
	return x%2==0
print("-----New list using filter()------")
#print(getEven(3))	
even_nums = filter(getEven, nums)
print(list(even_nums))
	
	
