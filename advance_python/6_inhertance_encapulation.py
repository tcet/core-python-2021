class Parent:
	def __init__(self):
		#protected member
		self._x = 2
		#private member
		self.__y = "priya"
		self.wish = "hello"
		#print(self.__y)

class Child(Parent):
	def __init__(self):
		Parent.__init__(self)
		print(self._x)
		print(self.wish)
		self.__privateFun()
		#print(self.__y)
	
	def __privateFun(self):
		print("it is private function of child class")
	
	def normalFun(self):
		print("normal function of child class")

obj = Child()
obj.normalFun()
#obj.__privateFun()
#print(vars(obj))

obj1 = Parent()
#print(vars(obj1))