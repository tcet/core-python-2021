import pymysql
import db_connection

connect = db_connection.dbConnect()

cur = connect.cursor()
cur.execute("select * from employee")
output = cur.fetchall()

#print(type(output)); #this will print tuple	
#print(output); 

for rec in output:
	print(rec);

# To close the connection
connect.close()