print(type(123));

print(type(3.14));

print(type("tcet"));

print(type(["orange", "mango", 1, 2, 3]));

print(type((1,2,3,4,5)));

print(type({'name':"Shweta", 'age': 22, 'city': "New Mumbai"}));

print(type({"Sun", "Mon", "Tue"}));
