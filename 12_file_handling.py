#open existing file
file = open("test.txt") 
content = file.read() #read all contents of the file
print(content)
print(file.readline()) # read 1st line to file
file.close() #close the open file

#open or create file in write mode (connent will be overwritten for an existing file)
file = open("student.txt","w")
n = file.write("hello")
print(n)
file.close()

file = open("student.txt","w")
file.write("Hii students")
file.close() 

#append the connents to the file
file = open("student.txt","a")
file.write(" welcome to python class" )
file.close()

f = open("student.txt","r")
print(f.read())

file.close()
